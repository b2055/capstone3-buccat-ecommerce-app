import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { Container } from 'react-bootstrap'
import { Fragment, useState, useEffect } from 'react'
import './App.css';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import LoginAccount from './pages/LoginAccount';
import Register from './pages/Register';
import BGSlider from './components/BGSlider';
import { UserProvider } from './UserContext'

import Illustrations from './pages/Illustrations';
import IllustrationsView from './pages/IllustrationsView';
import ViewUser from './pages/ViewUser';
import Checkout from './pages/Checkout';
import About from './pages/About';
import FeaturedArt from './pages/FeaturedArt';
import ViewPanel from './pages/ViewPanel';
import SellYourWork from './pages/SellYourWork';
import OrderHistory from './pages/OrderHistory';

function App() {
  // State hook for the user state that's defined for a global scope
  // Initialize an Object with properties from the localStorage
  const [user, setUser] = useState({
    id: null,
    email: null,
    nickname: null,
    gender: null,
    profilePicture: null,
    isAdmin: null,
    token: localStorage.getItem('token'),
    profilePictureLocal: localStorage.getItem('profilePictureLocal')
  })

  //function for clearing localStorage on Logout
  const unsetUser = () => {
    localStorage.clear()
  }

  //function when id is null but there is token then retrieve user details
  const retrieveUserDetails = (token) => {
    fetch('http://localhost:4000/api/users/user/details', {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => res.json())
      .then(data => {

        setUser({
          id: data._id,
          email: data.email,
          nickname: data.nickname,
          gender: data.gender,
          profilePicture: data.profilePicture,
          isAdmin: data.isAdmin,
          token: localStorage.getItem('token')
        })

        
        localStorage.setItem("profilePictureLocal", data.profilePicture)
      })
  }

  function checkLogin() {
    if (localStorage.getItem('token') !== null && user.id === null) {
      retrieveUserDetails(localStorage.getItem('token'))
    } 
  }


  useEffect(() => {
    checkLogin()
    console.log(user)
  })


  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Fragment>
        <BGSlider />
        <Router>

          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/logout" component={Logout} />
            <Route exact path="/loginAccount" component={LoginAccount} />
            <Route exact path="/register" component={Register} />
            <Route exact path="/illustrations" component={Illustrations} />
            <Route exact path="/illustrations/view/:illustrationId/:sellersId" component={IllustrationsView} />
            <Route exact path="/user-sell" component={SellYourWork} />

            <Route exact path="/checkout" component={Checkout} />
            <Route exact path="/order-history" component={OrderHistory} />
            <Route exact path="/featured-art" component={FeaturedArt} />
            <Route exact path="/view-profile/:userId" component={ViewUser} />
            <Route exact path="/about" component={About} />
            <Route exact path="/view-panel" component={ViewPanel} />
            <Route component={Error} />
          </Switch>

        </Router>
      </Fragment>
    </UserProvider>

  );
}

export default App;
