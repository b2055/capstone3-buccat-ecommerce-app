import React, { Fragment, useState, useEffect } from "react"
import { Form, Button, Container, Row, Col, Navbar, Modal, CloseButton } from "react-bootstrap"
import { NavLink, Link, useHistory } from 'react-router-dom'
import AppNavbarLogoOnly from '../components/AppNavbarLogoOnly'
import AvatarEditor from 'react-avatar-editor'
import Swal from 'sweetalert2'
import axios from 'axios'

const StepTwo = ({ nextStep, handleFormData, prevStep, values }) => {

    const [modalShow, setModalShow] = React.useState(false);
    const images = importAll(require.context('../images/swalBackdrop', false, /\.(gif|png|jpe?g|svg)$/));
    const history = useHistory()
    const { email, password, gender, nickname } = values;
    let profilePicture = null

    const [image, setImage] = useState()
    const [allowZoomOut, setAllowZoomOut] = useState(false)
    const [position, setPosition] = useState({ x: 0.5, y: 0.5 })
    const [scale, setScale] = useState(1)
    const [rotate, setRotate] = useState(0)
    const [width, setWidth] = useState(200)
    const [height, setHeight] = useState(200)
    const [isImageSelected, setIsImageSelected] = useState(false)
    const [editor, setEditor] = useState(null)

    const [previewImage, setPreviewImage] = useState()

    const [blobImage, setBlobImage] = useState(null)

    const setEditorRef = (editor) => (setEditor(editor))

    const handleNewImage = e => {
        setImage(e.target.files[0])
        setIsImageSelected(true)
        setModalShow(true)
    }

    const handleScale = e => {
        const scale2 = parseFloat(e.target.value)
        setScale(scale2)
    }

    const handlePositionChange = position => {
        setPosition(position)
    }

    function SaveChanges(e) {
        e.preventDefault()
        if (editor) {
            const url = editor.getImageScaledToCanvas().toDataURL();
            setBlobImage(editor.getImageScaledToCanvas())
            setPreviewImage(url)
        }
        setIsImageSelected(true)
        setModalShow(false)
    }

    function CancelChanges(e) {
        e.preventDefault()

        setImage()
        setIsImageSelected(false)
        document.getElementById("avatar-photo").value = ""
        setModalShow(false)
    }

    function CloseModal(e) {
        e.preventDefault()
        setModalShow(false)
    }

    useEffect(() => {

    })

    function importAll(r) {
        return r.keys().map(r);
    }

    const registerUser = (e) => {
        e.preventDefault()

        if (blobImage !== null) {
            blobImage.toBlob(function (blob) {
                const bodyFormData = new FormData();
                bodyFormData.append('file', blob, 'filename.png');
                bodyFormData.append('email', email);
                bodyFormData.append('password', password);
                bodyFormData.append('nickname', nickname);
                bodyFormData.append('gender', gender);

                axios.post('http://localhost:4000/api/users/register', bodyFormData)
                    .then((response) => {
                        if (response.data === true) {
                            Swal.fire({
                                title: "Successfully registered",
                                icon: "success",
                                text: "You have successfully registered an account.",
                                confirmButtonColor: 'rgb(189, 151, 98)',
                                color: 'white',
                                background: 'rgb(125, 125, 125)',
                                backdrop: `url("${images[1]}") right bottom no-repeat`,
                                showClass: {
                                    popup: 'animate__animated animate__fadeInUp animate__faster'
                                },
                                hideClass: {
                                    popup: 'animate__animated animate__fadeOutUp animate__faster'
                                }
                            })
                            history.push("/loginAccount")
                        } else {
                            Swal.fire({
                                title: "Something went wrong",
                                icon: "error",
                                text: "Please try again.",
                                confirmButtonColor: 'rgb(189, 151, 98)',
                                color: 'white',
                                background: 'rgb(125, 125, 125)',
                                backdrop: `url("${images[0]}") right bottom no-repeat`,
                                showClass: {
                                    popup: 'animate__animated animate__fadeInUp animate__faster'
                                },
                                hideClass: {
                                    popup: 'animate__animated animate__fadeOutUp animate__faster'
                                }
                            })
                        }
                    })
            });
        } else {
            const bodyFormData = new FormData();
            bodyFormData.append('email', email);
            bodyFormData.append('password', password);
            bodyFormData.append('nickname', nickname);
            bodyFormData.append('gender', gender);

            axios.post('http://localhost:4000/api/users/register', bodyFormData)
                .then((response) => {
                    if (response.data === true) {
                        Swal.fire({
                            title: "Successfully registered",
                            icon: "success",
                            text: "You have successfully registered an account.",
                            confirmButtonColor: 'rgb(189, 151, 98)',
                            color: 'white',
                            background: 'rgb(125, 125, 125)',
                            backdrop: `url("${images[1]}") right bottom no-repeat`,
                            showClass: {
                                popup: 'animate__animated animate__fadeInUp animate__faster'
                            },
                            hideClass: {
                                popup: 'animate__animated animate__fadeOutUp animate__faster'
                            }
                        })
                        history.push("/loginAccount")
                    } else {
                        Swal.fire({
                            title: "Something went wrong",
                            icon: "error",
                            text: "Please try again.",
                            confirmButtonColor: 'rgb(189, 151, 98)',
                            color: 'white',
                            background: 'rgb(125, 125, 125)',
                            backdrop: `url("${images[0]}") right bottom no-repeat`,
                            showClass: {
                                popup: 'animate__animated animate__fadeInUp animate__faster'
                            },
                            hideClass: {
                                popup: 'animate__animated animate__fadeOutUp animate__faster'
                            }
                        })
                    }
                })

        }

    }



    return (

        <Fragment>
            <AppNavbarLogoOnly />
            <Container className="register-container">
                <Row className="h-100 d-flex align-items-center justify-content-center p-0 m-0">
                    <Col className="register-box col-md-4 col-sm-12 h-75 d-flex p-0 m-0">
                        <Row className="w-100 p-0 m-0">
                            <Col className="col-12 d-flex justify-content-center align-items-center">
                                <Navbar.Brand className="text-white register-box-logo d-flex justify-content-center align-items-center" as={NavLink} to="/" exact>EMINA<small>MISE</small></Navbar.Brand>
                            </Col>
                            <Col className="col-12 d-flex justify-content-center">
                                <Row className="w-100 h-100 p-0 m-0">
                                    <h4 className="text-center">Create an Account</h4>
                                    <Col className="col-12">
                                        <Form onSubmit={(e) => registerUser(e)}>

                                            <Form.Group className="mb-5">
                                                <h5>Nickname</h5>
                                                <Form.Control
                                                    type="text"
                                                    placeholder="Nickname (Min. 4 characters)"
                                                    onChange={handleFormData("nickname")}
                                                    minLength="4"
                                                    required
                                                />
                                            </Form.Group>

                                            <Form.Group>

                                                <Row>
                                                    <Col className="col-12 d-flex justify-content-start border-bottom">
                                                        <h5 className="w-25">Gender: </h5>
                                                        <Form.Check
                                                            inline
                                                            label="Male"
                                                            type="radio"
                                                            value="Male"
                                                            name="genderRadio"
                                                            onChange={handleFormData("gender")}
                                                            className="text-white w-25"
                                                            required
                                                        />
                                                        <Form.Check
                                                            inline
                                                            label="Female"
                                                            type="radio"
                                                            value="Female"
                                                            name="genderRadio"
                                                            onChange={handleFormData("gender")}
                                                            className="text-white w-25"
                                                            required
                                                        />
                                                        <Form.Check
                                                            inline
                                                            label="Other"
                                                            type="radio"
                                                            value="Other"
                                                            name="genderRadio"
                                                            onChange={handleFormData("gender")}
                                                            className="text-white w-25"
                                                            required
                                                        />
                                                    </Col>
                                                </Row>

                                            </Form.Group>

                                            <Form.Group className="d-flex justify-content-center border-bottom">
                                                {
                                                    (isImageSelected === false) ?
                                                        <Row className="my-5">
                                                            <Col className="col-12 d-flex justify-content-center">
                                                                <h5>Avatar (optional)</h5>

                                                            </Col>
                                                            <Col>
                                                                <Form.Control
                                                                    onClick={() => setModalShow(true)}
                                                                    className="btn text-white btn-secondary"
                                                                    id="avatar-photo"
                                                                    name="newImage"
                                                                    accept="image/png, image/jpeg"
                                                                    type="file"
                                                                    onChange={(e) => handleNewImage(e)} />
                                                            </Col>
                                                        </Row>
                                                        :
                                                        <Fragment>
                                                            <Row className="my-5">
                                                                <Col className="col-12 d-flex justify-content-center">
                                                                    <h5>Avatar Preview</h5>

                                                                </Col>
                                                                <Col className="col-12 d-flex justify-content-center">

                                                                    <img src={previewImage} className="avatar-photo-preview rounded-circle" />
                                                                </Col>
                                                                <Col className="col-12 d-flex justify-content-center">
                                                                    <Button className="my-2" variant="primary" onClick={() => setModalShow(true)}>
                                                                        <Form.Control className="btn text-white btn-primary" id="avatar-photo" name="newImage" accept="image/png, image/jpeg" type="file" onChange={(e) => handleNewImage(e)} hidden />
                                                                        Change Avatar

                                                                    </Button>
                                                                </Col>
                                                            </Row>

                                                        </Fragment>
                                                }


                                            </Form.Group>

                                            <Row className="mt-3">
                                                <Col className="col-6">
                                                    <Button variant="secondary border-bottom w-100 h-100 rounded-0" onClick={prevStep}>
                                                        Previous
                                                    </Button>
                                                </Col>
                                                <Col className="col-6">
                                                    <Button variant="primary btn-login border-bottom w-100 h-100 rounded-0" type="submit">
                                                        Submit
                                                    </Button>
                                                </Col>
                                                <Col className="col-12 d-flex justify-content-center mt-2">
                                                    <Link className="text-center text-white" as={NavLink} to="/login">Go back</Link>
                                                </Col>
                                            </Row>



                                        </Form>
                                    </Col>

                                </Row>
                            </Col>
                            <Col className="col-12 d-flex justify-content-center align-items-center border-top">
                                <p className="text-white disclaimer-text">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lobortis lacus a tellus mollis, vitae fringilla turpis imperdiet. Donec at lobortis augue.
                                </p>

                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Container>
            <Modal
                className="avatar-modal"
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
                show={modalShow}
                onHide={() => setModalShow(false)}
            >
                <Modal.Header>

                    <Modal.Title id="contained-modal-title-vcenter">
                        Please select your avatar picture
                    </Modal.Title>
                    <CloseButton variant="white" onClick={(e) => CloseModal(e)} />
                </Modal.Header>

                <Modal.Body>
                    <Row>
                        <Col className="col-12 d-flex align-items-center justify-content-center">

                            <AvatarEditor
                                ref={(e) => setEditorRef(e)}
                                scale={parseFloat(scale)}
                                width={width}
                                height={height}
                                position={position}
                                onPositionChange={(e) => handlePositionChange(e)}
                                rotate={parseFloat(rotate)}
                                borderRadius={200}
                                image={image}
                                className="editor-canvas"
                            />

                        </Col>
                        <Col className="col-12 d-flex align-items-center justify-content-center mt-4">
                            <h4>Zoom</h4>
                            <input
                                name="scale"
                                type="range"
                                onChange={(e) => handleScale(e)}
                                min={allowZoomOut ? '0.1' : '1'}
                                max="2"
                                step="0.01"
                                defaultValue="1"
                            />
                        </Col>
                    </Row>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={(e) => CancelChanges(e)}>
                        Remove Image
                    </Button>
                    <Button variant="primary" onClick={(e) => SaveChanges(e)}>
                        Save Changes
                    </Button>
                </Modal.Footer>
            </Modal>
        </Fragment >


    );
};

export default StepTwo;