import { useState, Fragment, useContext, useEffect } from 'react'
import { Card, Col, Row, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function MyIllustrations({ illustrationProp }) {
    const { user, setUser } = useContext(UserContext);

    const { createdOn, description, isActive, isFeatured, likes, name, price, productPicture, sellerId, _id } = illustrationProp


    function unarchiveProduct(e, _id) {
        e.preventDefault()
        fetch(`http://localhost:4000/api/products/${_id}/unarchive`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${user.token}`
            }
        })
            .then(res => res.json())
            .then(data => {
                if (data == true) {
                    Swal.fire({
                        icon: 'success',
                        timer: 1000,
                        color: 'white',
                        background: 'rgb(125, 125, 125)'
                    })
                }
            })

    }

    useEffect(() => {

    })

    function archiveProduct(e, _id) {
        e.preventDefault()
        fetch(`http://localhost:4000/api/products/${_id}/archive`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${user.token}`
            }
        })
            .then(res => res.json())
            .then(data => {
                if (data == true) {
                    Swal.fire({
                        icon: 'success',
                        timer: 1000,
                        color: 'white',
                        background: 'rgb(125, 125, 125)'
                    })
                }
            })
    }

    return (
        <Fragment>

            <Col className="my-3 mx-2 col-lg-2 col-sm-12 p-0 m-0 d-flex justify-content-center">
                <Card className="card-product" style={{ width: '20rem', height: '30rem' }}>
                    <Card.Img
                        className="card-image-style rounded"
                        variant="top"
                        src={require(`../images/productPictures/${productPicture}`)} />
                    <Card.Body className="p-0 m-0 w-100">
                        <Row className="m-0 p-0 d-flex justify-content-center">
                            <Col className="col-12 d-flex justify-content-center">
                                <Link
                                    className="h4 w-100 m-0 p-0 card-title btn btn-secondary mt-2 text-white text-center"
                                    to={`illustrations/view/${_id}/${sellerId}`}>{name}
                                </Link>
                            </Col>

                            {
                                (isActive != true) ?
                                    <Col className="col-12 d-flex justify-content-center">
                                        <Button className="w-100" variant="primary" onClick={(e) => unarchiveProduct(e, _id)}>Unarchive</Button>
                                    </Col>
                                    :
                                    <Col className="col-12 mt-1
                                     d-flex justify-content-center">
                                        <Button className="w-100" variant="danger" onClick={(e) => archiveProduct(e, _id)}>Archive</Button>
                                    </Col>
                            }
                        </Row>
                    </Card.Body>
                </Card>
            </Col>
        </Fragment>
    )
}