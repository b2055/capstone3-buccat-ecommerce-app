import { Fragment, useEffect, useContext, useState } from 'react';
import { Container, Row, Col, Card, ButtonGroup, ToggleButton, } from 'react-bootstrap';
import AppNavbar from '../components/AppNavbar'
import ReactPaginate from 'react-paginate'
import axios from 'axios'
import UserContext from '../UserContext';
import Swal from 'sweetalert2'
import { Link } from 'react-router-dom'

export default function Illustrations() {

	const { user, setUser } = useContext(UserContext);

	const [illustrations2, setIllustrations2] = useState([])
	const [pageCount, setpageCount] = useState(0)
	const images = importAll(require.context('../images/swalBackdrop', false, /\.(gif|png|jpe?g|svg)$/));
	function importAll(r) {
		return r.keys().map(r);
	}
	let limitPage = 10


	const [showOwnIllustrations, setShowOwnIllustrations] = useState('1');

	const radios = [
		{ name: 'Show Own Illustration', value: '1' },
		{ name: 'Hide Own Illustration', value: '2' },
	];

	useEffect(() => {

		switch (showOwnIllustrations) {
			case "1":
				const getIllustrations = async () => {
					try {
						const res = await axios.get(`http://localhost:4000/api/products/page/1`);
						const totalRes = res.data.pop()
						const total = totalRes.totalAllResult;
						setpageCount(Math.ceil(total / limitPage));
						setIllustrations2(res.data);
					} catch (error) {
						console.error(error);
					}
				};
				getIllustrations();
				break;
			case "2":
				const config = {
					headers: { Authorization: `Bearer ${user.token}` }
				};
				const getIllustrations2 = async () => {
					try {
						const res = await axios.get(`http://localhost:4000/api/products/page/other/1`, config);
						const totalRes = res.data.pop()
						const total = totalRes.totalAllResult;
						setpageCount(Math.ceil(total / limitPage));
						setIllustrations2(res.data);
					} catch (error) {
						console.error(error);
					}
				};
				getIllustrations2();
				break;

			default:
				const getIllustrations3 = async () => {
					try {
						const res = await axios.get(`http://localhost:4000/api/products/page/1`);
						const totalRes = res.data.pop()
						const total = totalRes.totalAllResult;
						setpageCount(Math.ceil(total / limitPage));
						setIllustrations2(res.data);
					} catch (error) {
						console.error(error);
					}
				};
				getIllustrations3();
		}
	}, [])

	const fetchIllustrations = async (currentPage) => {
		try {
			const res = await axios.get(
				`http://localhost:4000/api/products/page/${currentPage}`
			);
			const data = res.data;
			return data;
		} catch (error) {
			console.error(error);
		}
	};

	const fetchIllustrations2 = async (currentPage) => {
		const config = {
			headers: { Authorization: `Bearer ${user.token}` }
		};
		try {
			const res = await axios.get(
				`http://localhost:4000/api/products/page/other/${currentPage}`, config
			);
			const data = res.data;
			return data;
		} catch (error) {
			console.error(error);
		}
	};

	const handlePageClick = async (data) => {

		let currentPage = data.selected + 1
		let illustrationsFormServer = ""

		switch (showOwnIllustrations) {
			case "1":
				illustrationsFormServer = await fetchIllustrations(currentPage)
				break;
			case "2":
				illustrationsFormServer = await fetchIllustrations2(currentPage)
				break;

			default:
				illustrationsFormServer = await fetchIllustrations(currentPage)
		}
		illustrationsFormServer.pop()
		setIllustrations2(illustrationsFormServer)
	}

	const showOwnIllustrationsRadio = async (value) => {
		setShowOwnIllustrations(value)

		switch (value) {
			case "1":
				const getIllustrations4 = async () => {
					try {
						const res = await axios.get(`http://localhost:4000/api/products/page/1`);
						const totalRes = res.data.pop()
						const total = totalRes.totalAllResult;
						setpageCount(Math.ceil(total / limitPage));
						setIllustrations2(res.data);
					} catch (error) {
						console.error(error);
					}
				};
				getIllustrations4();
				break;
			case "2":

				console.log("here")
				const config = {
					headers: { Authorization: `Bearer ${user.token}` }
				};
				const getIllustrations5 = async () => {
					try {
						const res = await axios.get(`http://localhost:4000/api/products/page/other/1`, config);
						const totalRes = res.data.pop()
						const total = totalRes.totalAllResult;

						console.log(total)
						console.log(limitPage)
						setpageCount(Math.ceil(total / limitPage));
						setIllustrations2(res.data);
					} catch (error) {
						console.error(error);
					}
				};
				getIllustrations5();
				break;

			default:
				const getIllustrations6 = async () => {
					try {
						const res = await axios.get(`http://localhost:4000/api/products/page/1`);
						const totalRes = res.data.pop()
						const total = totalRes.totalAllResult;
						setpageCount(Math.ceil(total / limitPage));
						setIllustrations2(res.data);
					} catch (error) {
						console.error(error);
					}
				};
				getIllustrations6();
		}


	}

	return (
		<Fragment>
			<AppNavbar />
			<Container fluid className="universal-container p-0 m-0 d-flex justify-content-center">
				<Row>
					<Col className="col-12 p-0 m-0">
						{
							(user.token == null) ?
								<></>
								:
								<ButtonGroup>
									{radios.map((radio, idx) => (


										<ToggleButton
											key={idx}
											id={`radio-${idx}`}
											type="radio"
											variant={idx % 2 ? 'outline-success' : 'outline-danger'}
											name="radio"
											value={radio.value}
											checked={showOwnIllustrations === radio.value}
											onChange={(e) => showOwnIllustrationsRadio(e.currentTarget.value)}
										>
											{radio.name}
										</ToggleButton>
									))}
								</ButtonGroup>
						}
					</Col>
				</Row>
				<Row className="p-0 m-0 w-100">
					<Col className="universal-box col-lg-12 col-md-12 col-sm-12 p-0 mt-2">
						<Row className="w-100 p-0 m-0 d-flex justify-content-center">
							<Col className="mt-3 col-12 p-0 m-0  d-flex justify-content-center">
								<h1>Illustration Canvas for sale by various artists.</h1>
							</Col>
							{
								illustrations2.map((illustration, i) => {
									return (
										<Fragment>
											<Col key={illustration.price + i} className="my-3 mx-2 col-lg-2 col-sm-12 p-0 m-0 d-flex justify-content-center">
												<Card key={illustration._id + i} className="card-product" style={{ width: '20rem', height: '30rem' }}>
													<Card.Img
														key={illustration.productPicture + i}
														className="card-image-style rounded"
														variant="top"
														src={require(`../images/productPictures/${illustration.productPicture}`)} />
													<Card.Body key={illustration.sellerId + i} className="p-0 m-0 d-flex justify-content-center">
														<Link
															key={illustration.name + i}
															className="h4 h-50 card-title btn btn-secondary mt-2 text-white text-center"
															to={`illustrations/view/${illustration._id}/${illustration.sellerId}`}>{illustration.name}</Link>
													</Card.Body>
												</Card>
											</Col>
										</Fragment>
									);
								})
							}

						</Row>
					</Col>
					{
						(pageCount !== 1) ?
							<Col className="col-12">
								<ReactPaginate
									previousLabel={'<'}
									nextLabel={'>'}
									breakLabel={'...'}
									pageCount={pageCount}
									marginePagesDisplayed={3}
									pageRangeDisplayed={3}
									onPageChange={handlePageClick}
									containerClassName={'pagination justify-content-center'}
									pageClassName={'page-item'}
									pageLinkClassName={'page-link'}
									previousClassName={'page-item'}
									previousLinkClassName={'page-link'}
									nextClassName={'page-item'}
									nextLinkClassName={'page-link'}
									breakClassName={'page-item'}
									breakLinkClassName={'page-link'}
									activeClassName={'active'}
								/>
							</Col> :
							<></>
					}
				</Row>
			</Container>
		</Fragment>
	)
}
