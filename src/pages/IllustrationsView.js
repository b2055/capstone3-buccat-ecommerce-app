import { Fragment, useEffect, useContext, useState } from 'react';
import { Container, Row, Col, Image, Button, Form } from 'react-bootstrap'
import AppNavbar from '../components/AppNavbar'
import { useParams, Link } from 'react-router-dom'
import Avatar from 'react-avatar'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function IllustrationsView() {

    const { user, setUser } = useContext(UserContext);
    const { illustrationId, sellersId } = useParams()
    const images = importAll(require.context('../images/swalBackdrop', false, /\.(gif|png|jpe?g|svg)$/));
    function importAll(r) {
        return r.keys().map(r);
    }
    //
    const [productId, setProductId] = useState()
    const [name, setName] = useState()
    const [description, setDescription] = useState()
    const [price, setPrice] = useState()
    const [isActive, setIsActive] = useState()
    const [isFeatured, setIsFeatured] = useState()
    const [productPicture, setProductPicture] = useState()
    const [productPictureFetch, setProductPictureFetch] = useState()
    const [likes, setLikes] = useState()
    const [createdOn, setCreatedOn] = useState()
    //
    const [sellerId, setSellerId] = useState()
    const [nickname, setNickname] = useState()
    const [profilePicture, setProfilePicture] = useState()
    const [quantity, setQuantity] = useState(1)
    //
    const [isOwner, setIsOwner] = useState(false)
    //

    function addToCart(e) {

        e.preventDefault()

        Swal.fire({
            title: "Add to Cart",
            icon: "question",
            text: "Add this illustration to your cart?",
            color: 'white',
            background: 'rgb(125, 125, 125)',
            backdrop: `url("${images[2]}") right bottom no-repeat`,
            showClass: {
                popup: 'animate__animated animate__fadeInUp animate__faster'
            },
            hideClass: {
                popup: 'animate__animated animate__fadeOutUp animate__faster'
            },
            showCancelButton: true,
            confirmButtonColor: 'rgb(189, 151, 98)',
            cancelButtonColor: 'rgb(115, 115, 115)',
            confirmButtonText: 'Confirm'
        }).then((result) => {
            if (result.isConfirmed) {

                fetch('http://localhost:4000/api/cart/addCart', {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                        Authorization: `Bearer ${user.token}`
                    },
                    body: JSON.stringify({
                        productId: productId,
                        quantity: quantity,
                        price: price,
                        sellerId: sellerId,
                        productPicture: productPictureFetch,
                        productName: name
                    })

                })
                    .then(res => res.json())
                    .then(data => {
                        if (data === true) {
                            Swal.fire({
                                icon: 'success',
                                timer: 1000,
                                color: 'white',
                                background: 'rgb(125, 125, 125)'
                            })

                            setQuantity(1)
                        } else {
                            Swal.fire({
                                title: "Something went wrong",
                                icon: "error",
                                text: "Please try again.",
                                confirmButtonColor: 'rgb(189, 151, 98)',
                                color: 'white',
                                background: 'rgb(125, 125, 125)',
                                backdrop: `url("${images[0]}") right bottom no-repeat`,
                                showClass: {
                                    popup: 'animate__animated animate__fadeInUp animate__faster'
                                },
                                hideClass: {
                                    popup: 'animate__animated animate__fadeOutUp animate__faster'
                                }
                            })
                        }

                    })

            }
        })
    }

    useEffect(() => {

        fetch(`http://localhost:4000/api/products/${illustrationId}`)
            .then(res => res.json())
            .then(data => {
                setProductId(data._id)
                setSellerId(data.sellerId)
                setName(data.name)
                setDescription(data.description)
                setPrice(data.price)
                setIsActive(data.isActive)
                setIsFeatured(data.isFeatured)
                setProductPictureFetch(data.productPicture)
                setProductPicture(require(`../images/productPictures/${data.productPicture}`))
                setLikes(data.likes)
                setCreatedOn(data.createdOn)
                if (data != null) {
                    fetch(`http://localhost:4000/api/users/details/${data.sellerId}`)
                        .then(res => res.json())
                        .then(data => {
                            console.log(data)
                            setNickname(data.nickname)
                            setProfilePicture(require(`../images/profilePictures/${data.profilePicture}`))
                        })
                }
            })
        setIsOwner(sellersId == user.id)
    }, [illustrationId, sellersId])

    return (
        <Fragment>
            <AppNavbar />
            <Container fluid className="universal-container p-0 m-0 d-flex justify-content-center">
                <Row className="w-100 d-flex align-items-center justify-content-center p-0 m-0">
                    <Col className="my-3 col-12 d-flex justify-content-center align-items-center">
                        <h1 className="text-center">{name}</h1>
                    </Col>
                    <Col className="col-12 d-flex justify-content-center">
                        <Row className=" h-100 p-0 m-0">
                            <Col className="col-6 d-flex justify-content-center">
                                <Image className="view-product-image" src={productPicture} />
                            </Col>

                            <Col className="col-4">
                                <Row>
                                    <Col className='col-1 d-flex justify-content-center align-items-center'>
                                        <Link to={`/view-profile/${sellerId}`}>
                                            <Avatar
                                                round={true}
                                                size="50"
                                                name={nickname}
                                                src={profilePicture} />
                                        </Link>
                                    </Col>
                                    <Col className="col-11 d-flex justify-content-start align-items-center">
                                        <h4>{nickname}</h4>
                                    </Col>
                                    <Col className='mt-3 col-12 d-flex justify-content-start align-items-center'>
                                        <h4>Price: {price}</h4>
                                    </Col>
                                    <Col className='mt-3 col-12 d-flex justify-content-start align-items-center'>
                                            <h4 className="mt-2">Description: {description}</h4>
                                        </Col>
                                        <Col className='mt-3 col-12 d-flex justify-content-start align-items-center'>
                                            <h4 className="mt-2">Description: {description}</h4>
                                        </Col>
                                </Row>




                                {
                                    (isOwner == true) ?
                                        <></>
                                        :
                                        <Form className="mt-3" onSubmit={(e) => addToCart(e)}>
                                            <Form.Group className="mb-3" controlId="quantity">
                                                <Form.Control
                                                    min="1"
                                                    type="number"
                                                    placeholder="Qty"
                                                    value={quantity}
                                                    onChange={(e) => setQuantity(e.target.value)}
                                                    required
                                                />
                                            </Form.Group>
                                            <Button type="submit">Buy this Illustration Canvas</Button>
                                        </Form>
                                }
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Container>
        </Fragment>
    )
}
