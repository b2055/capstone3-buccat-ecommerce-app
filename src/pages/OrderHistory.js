import { Fragment, useContext, useState, useEffect } from 'react';
import { Container, Row, Col, Table, Card } from 'react-bootstrap';
import AppNavbar from '../components/AppNavbar'
import UserContext from '../UserContext'
import axios from 'axios'

export default function OrderHistory() {
	const { user, setUser } = useContext(UserContext);
	const [orderHistory, setOrderHistory] = useState([])
	const [productsList, setProductsList] = useState([])

	useEffect(() => {

		const config = {
			headers: { Authorization: `Bearer ${user.token}` }
		};

		const getOrderHistory = async () => {
			try {
				const res = await axios.get(`http://localhost:4000/api/order/users/myOrders`, config);

				setProductsList(res.data)
				setOrderHistory(res.data)

			} catch (error) {
				console.error(error);
			}
		};
		getOrderHistory();


	}, [])

	return (

		<Fragment>
			<AppNavbar />

			<Container fluid className="universal-container">
				<Row className="d-flex justify-content-center ">
					<Col className="col-12">
						<h1 className="text-center mt-5">Order History</h1>
					</Col>
					<Col className="col-6">
						{
							orderHistory.map((data, i) => {
								
								return (
									<Fragment>
										<Table className="p-0 mt-5 table-dark" striped bordered hover>
											<thead>
												<tr>
													<th colSpan="5">Ordered when {data.purchasedOn}</th>
												</tr>
												<tr>
													<th className="text-center">Preview Image</th>
													<th className="text-center">Name</th>
													<th className="text-center">Price</th>
													<th className="text-center">Quantity</th>
													<th className="text-center">Subtotal</th>
												</tr>
											</thead>
											<tbody>
												{
													productsList[i].productsPurchased.map((products, x) => {
														let subtotal = products.price*products.quantity
														return (
														<tr id={`table-row-item`}>
															<td className="d-flex justify-content-center">
																<Card className="card-product" style={{ width: '10rem', height: '10rem' }}>
																	<Card.Img
																		className="checkout-preview-image rounded"
																		variant="top"
																		src={require(`../images/productPictures/${products.productPicture}`)} />
																</Card>
															</td>
															<td className="text-center">{products.productName}</td>
															<td className="text-center">{products.price}</td>
															<td className="text-center">{products.quantity}</td>
															<td className="text-center">{subtotal}</td>
														</tr>
														)
													})

												}

											</tbody>
											<tfoot>
												<tr>
													<td colSpan="5" id="total-number-text"><h4>Total: {data.totalAmount}</h4></td>
												</tr>
											</tfoot>
										</Table>
									</Fragment>

								)
							})
						}

					</Col>
				</Row>
			</Container>
		</Fragment >
	)
}
