import { Fragment } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import AppNavbar from '../components/AppNavbar'
import { NavLink, Link } from 'react-router-dom'

export default function Home() {

	return (

		<Fragment>
			<AppNavbar />

			<Container className="home-container d-flex justify-content-center align-items-center">

			
			<Row className="w-100 p-0 m-0 welcome-box">
				<Col className="col-12 p-0 m-0 d-flex justify-content-center align-items-center">
					<h1 className="text-white mt-3">Welcome</h1>
				</Col>
				<Col className="col-12 p-0 m-0 d-flex justify-content-center align-items-center">
					<h1 className="mb-4 text-white text-center">Anime Illustration Canvas shop</h1>
				</Col>
				<Col className="col-12 p-0 m-0 d-flex justify-content-center align-items-center">
					<p className="mb-4 mb-md-5 text-white text-center">Emina Mise is a online ecommerce for artists and art enthusiasts, and a platform for emerging and established artists to exhibit, promote, and sell their works with an enthusiastic, art-centric community.</p>
				</Col>
				<Col className="col-12 p-0 m-0 d-flex justify-content-center align-items-center">

						<Link className="btn btn-primary p-3 px-xl-4 py-xl-3 mb-3" as={NavLink} to="/illustrations" exact>Shop Now</Link>
				</Col>
			</Row>
			</Container>


		</Fragment>
	)
}
