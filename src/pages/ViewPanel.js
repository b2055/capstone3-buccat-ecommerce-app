import React, { Fragment, useState, useContext, useEffect } from 'react';
import { Form, Container, Row, Col, ToggleButton, ButtonGroup, Table, CloseButton, Button, Modal } from 'react-bootstrap';
import AppNavbar from '../components/AppNavbar'
import MyIllustrations from '../components/MyIllustrations'
import UserContext from '../UserContext'
import axios from 'axios'
import AvatarEditor from 'react-avatar-editor'


export default function ViewPanel() {
	const { user, setUser } = useContext(UserContext);
	const [showOwnIllustrations, setShowOwnIllustrations] = useState('1');

	const [femaleCheck, setFemaleCheck] = useState(false)
	const [maleCheck, setMaleCheck] = useState(false)
	const [otherCheck, setOtherCheck] = useState(false)
	//
	const [nickname, setNickname] = useState(user.nickname)
	const [profilePicture, setProfilePicture] = useState(require(`../images/profilePictures/${localStorage.getItem('profilePictureLocal')}`))
	const [gender, setGender] = useState(user.gender)
	const [email, setEmail] = useState(user.email)
	const [newPassword, setNewPassword] = useState(null)
	//
	const images = importAll(require.context('../images/swalBackdrop', false, /\.(gif|png|jpe?g|svg)$/));
	function importAll(r) {
		return r.keys().map(r);
	}

	const radios = [
		{ name: 'Details', value: '1' },
		{ name: 'Illustrations', value: '2' },
	];


	const [image, setImage] = useState()
	const [allowZoomOut, setAllowZoomOut] = useState(false)
	const [position, setPosition] = useState({ x: 0.5, y: 0.5 })
	const [scale, setScale] = useState(1)
	const [rotate, setRotate] = useState(0)
	const [width, setWidth] = useState(200)
	const [height, setHeight] = useState(200)
	const [modalShow, setModalShow] = React.useState(false);
	const [isImageSelected, setIsImageSelected] = useState(false)
	const [editor, setEditor] = useState(null)
	const [previewImage, setPreviewImage] = useState()
	const [blobImage, setBlobImage] = useState(null)
	const setEditorRef = (editor) => (setEditor(editor))

	//ILLUST
	const [illustrations, setIllustrations] = useState([])

	const handleNewImage = e => {
		setImage(e.target.files[0])
		setIsImageSelected(true)
		setModalShow(true)
	}

	const handleScale = e => {
		const scale2 = parseFloat(e.target.value)
		setScale(scale2)
	}

	const handlePositionChange = position => {
		setPosition(position)
	}

	function SaveChanges(e) {
		e.preventDefault()
		if (editor) {
			const url = editor.getImageScaledToCanvas().toDataURL();
			setBlobImage(editor.getImageScaledToCanvas())
			setPreviewImage(url)
		}
		setIsImageSelected(true)
		setModalShow(false)
	}

	function CancelChanges(e) {
		e.preventDefault()

		setImage()
		setIsImageSelected(false)
		document.getElementById("avatar-photo").value = ""
		setModalShow(false)
	}

	function CloseModal(e) {
		e.preventDefault()
		setModalShow(false)
	}

	function resetAvatar(e) {
		e.preventDefault()

		setImage()
		setIsImageSelected(false)
		document.getElementById("avatar-photo").value = ""
		setModalShow(false)
	}

	//USE EFFECT
	useEffect(() => {

		if (user.gender == null) {
			fetch('http://localhost:4000/api/users/user/details', {
				method: "GET",
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${user.token}`
				}
			})
				.then(res => res.json())
				.then(data => {
					if (data != null) {
						setNickname(data.nickname);
						setProfilePicture(require(`../images/profilePictures/${data.profilePicture}`));
						setGender(data.gender);
						setEmail(data.email);
						switch (gender) {
							case "Male":
								setMaleCheck(true)
								setFemaleCheck(false)
								setOtherCheck(false)
								break
							case "Female":
								setMaleCheck(false)
								setFemaleCheck(true)
								setOtherCheck(false)
								break
							case "Other":
								setMaleCheck(false)
								setFemaleCheck(false)
								setOtherCheck(true)
								break;
							default:

						}
					}
				})
		}

		switch (gender) {
			case "Male":
				setMaleCheck(true)
				setFemaleCheck(false)
				setOtherCheck(false)
				break
			case "Female":
				setMaleCheck(false)
				setFemaleCheck(true)
				setOtherCheck(false)
				break
			case "Other":
				setMaleCheck(false)
				setFemaleCheck(false)
				setOtherCheck(true)
				break;
			default:

		}

		const handleEsc = (event) => {
			if (event.keyCode === 27 && modalShow === true) {
				resetAvatar(event)
			}
		};
		window.addEventListener('keydown', handleEsc);

		return () => {
			window.removeEventListener('keydown', handleEsc);
		};
	})

	function showUserDetails() {
		fetch('http://localhost:4000/api/users/user/details', {
			method: "GET",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${user.token}`
			}
		})
			.then(res => res.json())
			.then(data => {
				if (data != null) {
					setNickname(data.nickname);
					setProfilePicture(require(`../images/profilePictures/${data.profilePicture}`));
					setGender(data.gender);
					setEmail(data.email);
					switch (gender) {
						case "Male":
							setMaleCheck(true)
							setFemaleCheck(false)
							setOtherCheck(false)
							break
						case "Female":
							setMaleCheck(false)
							setFemaleCheck(true)
							setOtherCheck(false)
							break
						case "Other":
							setMaleCheck(false)
							setFemaleCheck(false)
							setOtherCheck(true)
							break;
						default:

					}
				}
			})
	}


	////////////////////////////////////////////////////////////////////////////
	function showUserIllustrations() {
		fetch('http://localhost:4000/api/products/seller/MyProducts', {
			method: "GET",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${user.token}`
			}
		})
			.then(res => res.json())
			.then(data => {
				console.log(data)
				if (data != null) {
					setIllustrations(
						data.map(illustrations => {
							return (
								<MyIllustrations key={illustrations._id} illustrationProp={illustrations} />
							)
						}) 
					)
				}
			})
	}

	const showOwnIllustrationsRadio = async (value) => {
		setShowOwnIllustrations(value)

		switch (value) {
			case "1":
				showUserDetails()
				break;
			case "2":
				showUserIllustrations()
				break;
			default:

		}
	}

	function updateUser(e) {
		e.preventDefault()

		const config = {
			headers: { Authorization: `Bearer ${user.token}`, "Content-Type": "application/json" }
		};

		const bodyFormData = new FormData();
		if (user.email != email && email != undefined && email != null) {
			bodyFormData.append('email', email);

			setUser({
				id: user.id,
				email: email,
				nickname: user.nickname,
				gender: user.gender,
				profilePicture: localStorage.getItem('profilePictureLocal'),
				isAdmin: user.isAdmin,
				token: localStorage.getItem('token'),
				profilePictureLocal: localStorage.getItem('profilePictureLocal')
			  })
		}
		if (newPassword != undefined && newPassword != null && newPassword != "") {
			bodyFormData.append('password', newPassword);
		}
		if (user.nickname != nickname && nickname != undefined && nickname != null) {
			bodyFormData.append('nickname', nickname);

			setUser({
				id: user.id,
				email: user.email,
				nickname: nickname,
				gender: user.gender,
				profilePicture: localStorage.getItem('profilePictureLocal'),
				isAdmin: user.isAdmin,
				token: localStorage.getItem('token'),
				profilePictureLocal: localStorage.getItem('profilePictureLocal')
			  })

		}
		if (gender != user.gender && gender != undefined && gender != null) {
			bodyFormData.append('gender', gender);

			setUser({
				id: user.id,
				email: user.email,
				nickname: user.nickname,
				gender: gender,
				profilePicture: localStorage.getItem('profilePictureLocal'),
				isAdmin: user.isAdmin,
				token: localStorage.getItem('token'),
				profilePictureLocal: localStorage.getItem('profilePictureLocal')
			  })
		}

		if (blobImage !== null) {
			blobImage.toBlob(function (blob) {

				bodyFormData.append('file', blob, 'filename.png');
				axios.put('http://localhost:4000/api/users/update', bodyFormData, config)
					.then((response) => {

						console.log(response.data)
						if (response.data === true) {

						} else {

						}
					})
			});
		} else {

			axios.put('http://localhost:4000/api/users/update', bodyFormData, config)
				.then((response) => {

					console.log(response.data)
					if (response.data === true) {

					} else {

					}
				})
		}
	}

	return (

		<Fragment>
			<AppNavbar />

			<Container fluid className="universal-container">
				<Row className="d-flex justify-content-center">
					<Col className="mt-4 col-12 d-flex justify-content-center">
						<ButtonGroup>
							{radios.map((radio, idx) => (
								<ToggleButton
									key={idx}
									id={`radio-${idx}`}
									type="radio"
									variant={idx % 2 ? 'secondary' : 'primary'}
									name="radio"
									value={radio.value}
									checked={showOwnIllustrations === radio.value}
									onChange={(e) => showOwnIllustrationsRadio(e.currentTarget.value)}
								>
									{radio.name}
								</ToggleButton>
							))}
						</ButtonGroup>
					</Col>
					
						{
							(showOwnIllustrations == 1) ?
								<Fragment>
									<Col className="col-6">
									<h1 className="text-center mt-5">User Details</h1>
									<Form onSubmit={(e) => updateUser(e)}>
										<Table className="p-0 m-0 table-dark" striped bordered hover>
											<tbody>
												<tr>
													<th className="text-center">Email</th>
													<td className="text-center">
														<Form.Group className="mb-3">
															<Form.Control
																name="email"
																defaultValue={user.email}
																onChange={e => setEmail(e.target.value)}
																type="email"
																required
															/>
														</Form.Group>
													</td>
												</tr>
												<tr>
													<th className="text-center">Password</th>
													<td className="text-center">
														<Form.Group className="mb-3">
															<Form.Control
																name="password"
																type="password"
																placeholder="Set New Password (Min. 8 characters)"
																minLength="8"
																onChange={e => setNewPassword(e.target.value)}
															/>
														</Form.Group>
													</td>
												</tr>
												<tr>
													<th className="text-center">Nickname</th>
													<td className="text-center">
														<Form.Control
															type="text"
															defaultValue={user.nickname}
															onChange={e => setNickname(e.target.value)}
															minLength="4"
															required
														/>
													</td>
												</tr>
												<tr>
													<th className="text-center">Gender</th>
													<td className="text-center">
														<Form.Group>
															<Row>
																<Col className="col-12 d-flex justify-content-center">
																	<Form.Check
																		inline
																		label="Male"
																		type="radio"
																		value="Male"
																		name="genderRadio"
																		checked={maleCheck}
																		onChange={(e) => setGender(e.target.value)}
																		className="text-white w-25"
																		required
																	/>
																	<Form.Check
																		inline
																		label="Female"
																		type="radio"
																		value="Female"
																		name="genderRadio"
																		checked={femaleCheck}
																		onChange={(e) => setGender(e.target.value)}
																		className="text-white w-25"
																		required
																	/>
																	<Form.Check
																		inline
																		label="Other"
																		type="radio"
																		value="Other"
																		name="genderRadio"
																		checked={otherCheck}
																		onChange={(e) => setGender(e.target.value)}
																		className="text-white w-25"
																		required
																	/>
																</Col>
															</Row>
														</Form.Group>
													</td>
												</tr>
												<tr>
													<th className="text-center">Avatar</th>
													<td className="text-center">
														{
															(isImageSelected === false) ?
																<Fragment>
																	<Col className="col-12 mt-2 d-flex justify-content-center">
																		<img
																			src={profilePicture}
																			className="avatar-photo-preview rounded-circle" />
																	</Col>
																</Fragment>
																:
																<Fragment>
																	<Col className="col-12 mt-2 d-flex justify-content-center">
																		<img
																			src={previewImage}
																			className="avatar-photo-preview rounded-circle" />
																	</Col>
																	<Col className="col-12 d-flex justify-content-center">
																		<Button className="mt-2 ml-5"
																			variant="secondary"
																			onClick={(e) => resetAvatar(e)}
																		>Reset</Button>
																	</Col>
																</Fragment>
														}
														<Col className="col-12 d-flex justify-content-center">
															<Form.Control
																className="mt-2 w-25 btn text-white btn-secondary mr-2"
																id="avatar-photo"
																name="newImage"
																accept="image/png, image/jpeg"
																type="file"
																onClick={() => setModalShow(true)}
																onChange={(e) => handleNewImage(e)}
															/>
														</Col>
													</td>
												</tr>
											</tbody>
											<tfoot>
												<tr>
													<td colSpan="2" className="">
														<Col className="col-12 d-flex justify-content-center">
															<Button type="submit" className="btn btn-secondary">Save Changes</Button>
														</Col>
													</td>
												</tr>
											</tfoot>
										</Table>
									</Form>
									</Col>
								</Fragment>
								:	
								<Fragment>
									<h1 className="text-center mt-5">Illustrations</h1>
									{illustrations}

								</Fragment>
}
						
					
				</Row>
			</Container>

			<Modal
				data-backdrop="static"
				data-keyboard="false"
				className="avatar-modal"
				size="lg"
				aria-labelledby="contained-modal-title-vcenter"
				centered
				show={modalShow}
				onHide={() => setModalShow(false)}
			>
				<Modal.Header>

					<Modal.Title id="contained-modal-title-vcenter">
						Please select your avatar picture
					</Modal.Title>
					<CloseButton variant="white" onClick={(e) => CancelChanges(e)} />
				</Modal.Header>

				<Modal.Body>
					<Row>
						<Col className="col-12 d-flex align-items-center justify-content-center">

							<AvatarEditor
								ref={(e) => setEditorRef(e)}
								scale={parseFloat(scale)}
								width={width}
								height={height}
								position={position}
								onPositionChange={(e) => handlePositionChange(e)}
								rotate={parseFloat(rotate)}
								borderRadius={200}
								image={image}
								className="editor-canvas"
							/>

						</Col>
						<Col className="col-12 d-flex align-items-center justify-content-center mt-4">
							<h4>Zoom</h4>
							<input
								name="scale"
								type="range"
								onChange={(e) => handleScale(e)}
								min={allowZoomOut ? '0.1' : '1'}
								max="2"
								step="0.01"
								defaultValue="1"
							/>
						</Col>
					</Row>
				</Modal.Body>
				<Modal.Footer>
					<Button variant="secondary" onClick={(e) => CancelChanges(e)}>
						Cancel
					</Button>
					<Button variant="primary" onClick={(e) => SaveChanges(e)}>
						Save Changes
					</Button>
				</Modal.Footer>
			</Modal>


		</Fragment>
	)
}
