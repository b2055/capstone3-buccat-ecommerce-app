import { Fragment, useState, useContext } from 'react';
import { Container, Row, Col, Form, Button, Image } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import AppNavbar from '../components/AppNavbar'
import UserContext from '../UserContext';
import Swal from 'sweetalert2'
import axios from 'axios'

export default function SellYourWork() {

    const { user, setUser } = useContext(UserContext);
    const images = importAll(require.context('../images/swalBackdrop', false, /\.(gif|png|jpe?g|svg)$/));
    function importAll(r) {
        return r.keys().map(r);
    }
    const [productName, setProductName] = useState();
    const [productDescription, setProductDescription] = useState();
    const [price, setPrice] = useState();
    const [productPicturePreview, setProductPicturePreview] = useState();
    const [productPicture, setProductPicture] = useState();
    const onChangePicture = (e) => {
        setProductPicturePreview(URL.createObjectURL(e.target.files[0]))
        setProductPicture(e.target.files[0])
    }
    const submitFormData = (e) => {
        e.preventDefault();

        const bodyFormData = new FormData();
        bodyFormData.append('name', productName);
        bodyFormData.append('description', productDescription);
        bodyFormData.append('price', price);
        bodyFormData.append('file', productPicture);

        const config = {
            headers: { Authorization: `Bearer ${user.token}` }
        };

        axios.post('http://localhost:4000/api/products', bodyFormData, config)
            .then((response) => {
                if (response.data === true) {
                    Swal.fire({
                        title: "Successfully posted",
                        icon: "success",
                        text: "You have successfully posted your work for sale.",
                        confirmButtonColor: 'rgb(189, 151, 98)',
                        color: 'white',
                        background: 'rgb(125, 125, 125)',
                        backdrop: `url("${images[1]}") right bottom no-repeat`,
                        showClass: {
                            popup: 'animate__animated animate__fadeInUp animate__faster'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__fadeOutUp animate__faster'
                        }
                    })

                    setProductName("")
                    setProductDescription("")
                    setPrice("")
                    setProductPicturePreview("")
                    setProductPicture("")
                    document.getElementById("product-photo").value = ""
                } else {
                    Swal.fire({
                        title: "Something went wrong",
                        icon: "error",
                        text: "Please try again.",
                        confirmButtonColor: 'rgb(189, 151, 98)',
                        color: 'white',
                        background: 'rgb(125, 125, 125)',
                        backdrop: `url("${images[0]}") right bottom no-repeat`,
                        showClass: {
                            popup: 'animate__animated animate__fadeInUp animate__faster'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__fadeOutUp animate__faster'
                        }
                    })
                }
            })
    };


    return (
        (user.token == null) ?
            <Redirect to="/login" />
            :
            <Fragment>
                <AppNavbar />
                <Container fluid className="universal-container p-0 m-0 d-flex align-items-center justify-content-center">
                    <Row className="mt-5 w-50 h-75 d-flex align-items-center justify-content-center p-0 m-0">
                        <Col className="w-100 universal-box col-md-4 col-sm-12 h-100 d-flex p-0 m-0">
                            <Row className="w-100 p-0 m-0">
                                <Col className="mb-3 col-12 d-flex justify-content-center align-items-center">
                                    <h1 className="text-center">Sell your Illustration Canvas</h1>
                                </Col>
                                <Col className="col-12 d-flex justify-content-center">
                                    <Row className="w-100 h-100 p-0 m-0">
                                        <Col className="col-12">
                                            <Form.Group className="mb-3">
                                                <Row className="mb-5">
                                                    <Col className="col-12 d-flex justify-content-center">
                                                        <h5>Preview</h5>
                                                    </Col>
                                                    <Col className="col-12 d-flex justify-content-center">
                                                        <Image
                                                            thumbnail src={productPicturePreview}
                                                            className="avatar-photo-preview img-fluid"
                                                            style={{ width: "500px" }}
                                                        />
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col className="col-12 d-flex justify-content-center">
                                                        <h5>Illustration (Required)</h5>
                                                    </Col>
                                                    <Col className="d-flex justify-content-center align-items-center">
                                                        <Form.Control
                                                            onChange={onChangePicture}
                                                            className="w-25 btn text-white btn-secondary"
                                                            id="product-photo"
                                                            name="newImage"
                                                            accept="image/png, image/jpeg"
                                                            type="file" required
                                                        />
                                                    </Col>
                                                </Row>
                                            </Form.Group>
                                            <Form onSubmit={submitFormData}>
                                                <Row>
                                                    <Col className="col-6">
                                                        <Form.Group className="mb-3">
                                                            <Form.Control
                                                                name="productName"
                                                                type="text"
                                                                placeholder="Name"
                                                                value={productName}
                                                                onChange={(e) => setProductName(e.target.value)}
                                                                required
                                                            />
                                                        </Form.Group>
                                                    </Col>
                                                    <Col className="col-6">
                                                        <Form.Group className="mb-3">
                                                            <Form.Control
                                                                name="productPrice"
                                                                type="number"
                                                                placeholder="Price"
                                                                value={price}
                                                                onChange={(e) => setPrice(e.target.value)}
                                                                required
                                                            />
                                                        </Form.Group>
                                                    </Col>
                                                </Row>
                                                <Form.Group className="mb-3">
                                                    <Form.Control
                                                        name="productDescription"
                                                        type="text"
                                                        as="textarea"
                                                        placeholder="Description"
                                                        value={productDescription}
                                                        onChange={(e) => setProductDescription(e.target.value)}
                                                        required
                                                    />
                                                </Form.Group>
                                                <Row className='mt-5 mb-5'>
                                                    <Col className="col-12 d-flex justify-content-center">
                                                        <Button variant="primary btn-login border-bottom w-25 h-100 rounded-0" type="submit">
                                                            Post
                                                        </Button>
                                                    </Col>
                                                </Row>
                                            </Form>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Container>


            </Fragment>
    )
}
