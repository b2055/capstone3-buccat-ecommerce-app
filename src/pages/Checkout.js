import { Fragment, useState, useEffect, useContext } from 'react';
import { Container, Row, Col, Table, Button, Card } from 'react-bootstrap';
import { useHistory } from 'react-router-dom'
import AppNavbar from '../components/AppNavbar'
import UserContext from '../UserContext'
import axios from 'axios'
import Swal from 'sweetalert2'

export default function Checkout() {
	const { user, setUser } = useContext(UserContext);
	const history = useHistory()
	//
	const images = importAll(require.context('../images/swalBackdrop', false, /\.(gif|png|jpe?g|svg)$/));
	function importAll(r) {
		return r.keys().map(r);
	}

	const [cartItems, setCartItems] = useState([])
	let total = 0;

	useEffect(() => {

		const config = {
			headers: { Authorization: `Bearer ${user.token}` }
		};
		const getCart = async () => {
			try {
				const res = await axios.get(`http://localhost:4000/api/cart`, config);
				setCartItems(res.data);
			} catch (error) {
				console.error(error);
			}
		};
		getCart();


	}, [])

	function calculateTotal(e, index, price) {
		e.preventDefault()
		let subtotal = e.target.value * price
		var x = document.getElementsByClassName(`subtotal-number${index}`)
		x[0].innerHTML = subtotal

		var y = document.getElementById(`total-number-text`)
		
	}

	function saveQuantity(e, index, cartId, price) {
		e.preventDefault()

		let originalInput = document.getElementById(`originalInput${index}`).value

		if (originalInput !== e.target.value) {

			let newQuantity = e.target.value
			document.getElementById(`originalInput${index}`).value = newQuantity

			console.log(newQuantity - originalInput)

			let difference = newQuantity - originalInput;

			if (difference > 0) {
				total = (total + (difference * price))
			} else if (difference < 0) {
				total = (total - Math.abs(difference * price))
			}

			var x = document.getElementById('total-number-text')

			x.innerHTML = `Total: ${total}`

			console.log(document.getElementById('total-number-text').innerHtml)
			fetch(`http://localhost:4000/api/cart/${cartId}/changeQuantity`, {
				method: "PUT",
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${user.token}`
				},
				body: JSON.stringify({
					quantity: newQuantity
				})
			})
		}
	}

	function removeCartItem(e, index, cartId, price) {
		e.preventDefault()

		fetch(`http://localhost:4000/api/cart/${cartId}/removeCartItem`, {
			method: "DELETE",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${user.token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			if (data == true) {
				

				let originalInput = document.getElementById(`originalInput${index}`).value
				let priceDeduct = originalInput * price
				total = total - priceDeduct

				var x = document.getElementById('total-number-text')
				x.innerHTML = `Total: ${total}`
				document.getElementById(`table-row-item${index}`).remove()
			}

		})
	}

	function checkoutCart(e) {
		e.preventDefault()

		Swal.fire({
            title: "Checkout",
            icon: "question",
            text: "Are you sure you want to Checkout?",
            color: 'white',
            background: 'rgb(125, 125, 125)',
            backdrop: `url("${images[2]}") right bottom no-repeat`,
            showClass: {
                popup: 'animate__animated animate__fadeInUp animate__faster'
            },
            hideClass: {
                popup: 'animate__animated animate__fadeOutUp animate__faster'
            },
            showCancelButton: true,
            confirmButtonColor: 'rgb(189, 151, 98)',
            cancelButtonColor: 'rgb(115, 115, 115)',
            confirmButtonText: 'Confirm'
        }).then((result) => {
            if (result.isConfirmed) {

				fetch(`http://localhost:4000/api/order/users/checkout`, {
					method: "POST",
					headers: {
						"Content-Type": "application/json",
						Authorization: `Bearer ${user.token}`
					}
				})
				.then(res => res.json())
				.then(data => {
					if (data == true) {
						Swal.fire({
							icon: 'success',
							timer: 1000,
							color: 'white',
							background: 'rgb(125, 125, 125)'
						})	
						
						history.push("/order-history")
					} else {
						console.log(data)
					}
				})
            }
        })




	}

	return (
		<Fragment>
			<AppNavbar />
			<Container fluid className="universal-container">
				<Row className="d-flex justify-content-center ">
					<Col className="col-12">
						<h1 className="text-center mt-5">Your shopping cart</h1>
					</Col>
					<Col className="col-6 border">
						<div className="table-responsive">


							<Table className="p-0 m-0 table-dark" striped bordered hover>
								<thead>
									<tr>
										<th className="text-center">Preview Image</th>
										<th className="text-center">Name</th>
										<th className="text-center">Price</th>
										<th className="text-center">Quantity</th>
										<th className="text-center">Subtotal</th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									{
										cartItems.map((items, i) => {

											let subtotal = items.price * items.quantity

											total = total + subtotal

											return (
												<Fragment>
													<tr key={i} id={`table-row-item${i}`}>
														<td className="d-flex justify-content-center">
															<Card className="card-product" style={{ width: '10rem', height: '10rem' }}>
																<Card.Img
																	className="checkout-preview-image rounded"
																	variant="top"
																	src={require(`../images/productPictures/${items.productPicture}`)} />
															</Card>
														</td>
														<td className="text-center">{items.productName}</td>
														<td className="text-center">{items.price}</td>
														<td className="text-center w-25" key={items._id}>
															<input id={`changeInput${i}`} type="number" onBlur={(e) => saveQuantity(e, i, items._id, items.price)} defaultValue={items.quantity} onChange={(e) => calculateTotal(e, i, items.price)} />
															<input id={`originalInput${i}`} defaultValue={items.quantity} hidden />
														</td>
														<td className={`text-center subtotal-number${i}`}>{subtotal}</td>
														<td className="text-center"><Button variant="danger" onClick={(e) => removeCartItem(e, i, items._id, items.price)}>Remove</Button></td>
													</tr>
												</Fragment>
											)
										})
									}
								</tbody>
								<tfoot>
									<tr>
										<td colSpan="4" className="text-center">
											<td className="text-center w-100 checkout-button">
												<Button variant="success" className="w-100" onClick={(e) => checkoutCart(e)}>Checkout</Button>
											</td>
										</td>
										<td colSpan="2" id="total-number-text">Total: {total}</td>
									</tr>
								</tfoot>
							</Table>
						</div>
					</Col>
				</Row>
			</Container>
		</Fragment>
	)
}
